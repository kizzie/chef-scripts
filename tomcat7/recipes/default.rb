#
# Cookbook Name:: tomcat7
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

package 'tomcat7'

#change the tomcat-users.xml file
file '/etc/tomcat7/tomcat-users.xml' do
	source 'tomcat-users.xml'
end

#start the service
service 'tomcat7' do
  supports :status => true
  action [:enable, :start]
end

